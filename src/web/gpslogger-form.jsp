<%@ page
   import="org.jivesoftware.openfire.XMPPServer,
           org.jivesoftware.openfire.plugin.GPSLoggerPlugin,
           org.jivesoftware.util.ParamUtils,
           java.util.HashMap,
           java.util.Map,
           java.lang.Exception"
   errorPage="error.jsp"%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
	boolean save = request.getParameter("save") != null;	
	boolean gpsloggerEnabled = ParamUtils.getBooleanParameter(request, "gpsloggerEnabled", false);
	String gpsloggerInterval = ParamUtils.getParameter(request, "gpsloggerInterval");
    
	GPSLoggerPlugin plugin = (GPSLoggerPlugin) XMPPServer.getInstance().getPluginManager().getPlugin("gpslogger");

	Map<String, String> errors = new HashMap<String, String>();	
	if (save) {
		try {
			if(Integer.parseInt(gpsloggerInterval) <= 0) throw new Exception();
		}
		catch(Exception e) {
			errors.put("missingGpsloggerInterval", "missingGpsloggerInterval");
		}

	  if (errors.size() == 0) {
	     plugin.setEnabled(gpsloggerEnabled);
	     plugin.setInterval(gpsloggerInterval);
           
	     response.sendRedirect("gpslogger-form.jsp?settingsSaved=true");
	     return;
	  }		
	}
    
	gpsloggerEnabled = plugin.isEnabled();
	gpsloggerInterval = plugin.getInterval();
%>

<html>
	<head>
	  <title><fmt:message key="gpslogger.title" /></title>
	  <meta name="pageID" content="gpslogger-form"/>
	</head>
	<body>

<form action="gpslogger-form.jsp?save" method="post">

<div class="jive-contentBoxHeader"><fmt:message key="gpslogger.options" /></div>
<div class="jive-contentBox">
   
	<% if (ParamUtils.getBooleanParameter(request, "settingsSaved")) { %>
   
	<div class="jive-success">
	<table cellpadding="0" cellspacing="0" border="0">
	<tbody>
	  <tr>
	     <td class="jive-icon"><img src="images/success-16x16.gif" width="16" height="16" border="0"></td>
	     <td class="jive-icon-label"><fmt:message key="gpslogger.saved.success" /></td>
	  </tr>
	</tbody>
	</table>
	</div>
   
	<% } %>
   
	<table cellpadding="3" cellspacing="0" border="0" width="100%">
	<tbody>
	  <tr>
	     <td width="1%" align="center" nowrap><input type="checkbox" name="gpsloggerEnabled" <%=gpsloggerEnabled ? "checked" : "" %>></td>
	     <td width="99%" align="left"><fmt:message key="gpslogger.enable" /></td>
	  </tr>
	</tbody>
	</table>
   
   <br><br>
	<p><fmt:message key="gpslogger.directions" /></p>
   
	<table cellpadding="3" cellspacing="0" border="0" width="100%">
	<tbody>
	  <tr>
	     <td width="5%" valign="top"><fmt:message key="gpslogger.interval" />:&nbsp;</td>
	     <td width="95%"><textarea cols="45" rows="5" wrap="virtual" name="gpsloggerInterval"><%= gpsloggerInterval %></textarea></td>
	     <% if (errors.containsKey("missingGpsloggerInterval")) { %>
	        <span class="jive-error-text"><fmt:message key="gpslogger.interval.missing" /></span>
	     <% } %>            
	  </tr>
	</tbody>
	</table>
</div>
<input type="submit" value="<fmt:message key="gpslogger.button.save" />"/>
</form>

</body>
</html>
