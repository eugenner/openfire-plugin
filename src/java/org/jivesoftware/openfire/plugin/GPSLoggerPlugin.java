package org.jivesoftware.openfire.plugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.interceptor.InterceptorManager;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.jivesoftware.util.JiveGlobals;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;
import org.xmpp.packet.Presence;

/**
 * GPS Logger plugin.
 * 
 * @author <a href="mailto:eugenner@gmail.com">Eugene Bolshakov</a>
 */
public class GPSLoggerPlugin implements Plugin, PacketInterceptor {
	private static final String INTERVALPROP = "plugin.gpslogger.interval";
	private static final String ENABLED = "plugin.gpslogger.enabled";
	private XMPPServer server = XMPPServer.getInstance();
	private long lastPacketTime = -1; 
	private BufferedWriter out;
	
	public void initializePlugin(PluginManager manager, File pluginDirectory) {
		File file = new File(JiveGlobals.getHomeDirectory() 
        		+ File.separator + "logs" + File.separator, "gpslogger.log");
        if (!file.exists()) {
            try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        try {
			out = new BufferedWriter(new FileWriter(file, true));
		} catch (IOException e) {
			e.printStackTrace();
		}
		InterceptorManager.getInstance().addInterceptor(this);
	}

	public void destroyPlugin() {
		InterceptorManager.getInstance().removeInterceptor(this);
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void interceptPacket(Packet packet, Session session,
			boolean incoming, boolean processed) throws PacketRejectedException {
		if (!processed && incoming) {
			if(packet instanceof Presence && ((Presence)packet).getType() != Presence.Type.unavailable) {
				long currTime = Calendar.getInstance().getTimeInMillis();
				long delta = currTime - lastPacketTime;
				if("coordinates".equals(((Presence)packet).getStatus())) {
					String lat = packet.getElement().node(0).valueOf("/presence/coordinates/lat");
					String lng = packet.getElement().node(0).valueOf("/presence/coordinates/lon");
					String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
						.format(Calendar.getInstance().getTime());
					logToFile(timeStamp + " From: " + packet.getFrom() 
							+ " status: " + ((Presence)packet).getStatus()
							+ " Lat: " + lat + " Long: " + lng 
							+ (lastPacketTime != -1 ? " setted_interval: " + getInterval() + "000"
									+ " real_interval: " + delta : ""));
				} else {
					logToFile("From: " + packet.getFrom() 
							+ " status: " + ((Presence)packet).getStatus());
				}
				lastPacketTime = currTime;
				Message echo = new Message();
				echo.setTo(packet.getFrom());
				echo.setFrom(packet.getTo());
				echo.setSubject("interval");
				echo.setBody(getInterval());
				server.getPacketRouter().route(echo);
			}
		}
	}
	
	public void setInterval(String val) {
		JiveGlobals.setProperty(INTERVALPROP, val);
	}

	public String getInterval() {
		return JiveGlobals.getProperty(INTERVALPROP, "5");
	}

	public void setEnabled(boolean enable) {
		JiveGlobals.setProperty(ENABLED, enable ? Boolean.toString(true)
				: Boolean.toString(false));
	}

	public boolean isEnabled() {
		return JiveGlobals.getBooleanProperty(ENABLED, false);
	}

	public class CustomPresence extends org.xmpp.packet.Presence {

		private String customStanza;

		public void setCustomStanza(String customStanza) {
			this.customStanza = customStanza;
		}
		
		@Override
		public String toXML() {
			String originalXML = super.toXML();
			return originalXML.substring(0, originalXML.length() - 11) 
					+ customStanza +"</presence>";
		}
	}
	
	private void logToFile(String line) {
		try {
			out.write(line);
			out.newLine();
			out.flush();
		} catch (IOException e) {
			// TODO 
			e.printStackTrace();
		}
	}
}
